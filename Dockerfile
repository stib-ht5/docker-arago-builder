FROM ubuntu:12.04

# UK mirror
RUN sed -i 's|http://archive.ubuntu.com|http://gb.archive.ubuntu.com|' /etc/apt/sources.list

# reduce apt-get install warnings
ENV TERM xterm

# Platform requirements
#
# including i386 support
#
# debconf: delaying package configuration, since apt-utils is not installed
#
RUN apt-get update && apt-get install --no-install-recommends -y \
	apt-utils \
	build-essential \
	ca-certificates \
	git \
	libstdc++6:i386 \
	&& apt-get clean

# our `bb` expects /bin/sh to be bash
RUN echo "dash dash/sh boolean false" | debconf-set-selections && \
	dpkg-reconfigure -p critical dash

# required by OE's sanity check
RUN apt-get update && apt-get upgrade -y && apt-get install -y \
	chrpath \
	cvs \
	diffstat \
	gawk \
	subversion \
	texinfo \
	texi2html \
	&& apt-get clean

# build time depedencies
# python2.7-minimal isn't enough for bitbucket
# htc needs to run a lua script
# iptft:do_install() needs rsync
RUN apt-get update && apt-get upgrade -y && apt-get install -y \
	python2.7 \
	rsync \
	wget \
	lua5.1 \
	zip \
	&& apt-get clean

COPY updateconfig.sh /usr/local/bin/
COPY entrypoint.sh /

ENTRYPOINT [ "/entrypoint.sh" ]
