#!/bin/sh

# sanity
[ -s configure.ac -a -n "`git rev-parse --show-toplevel`" ] || exit 0

set -eu

# version
version=$(git describe --tags || echo 'no_version')
if [ "$version" = "${version%-dirty}" ]; then
	# git describe might consider clean something that's dirty anyway
	if [ -n "$(git ls-files -dm | grep -v configure.ac)" ]; then
		version="${version}-dirty"
	fi
fi
version_tag_ref="refs/tags/${version%-dirty}"

branch="$(git branch | sed -ne 's/^\* \+//p')"
case "$branch" in
"(no branch)"|"(HEAD detached from "*)
	# detached branch, cheap attempt to discovery it
	branch="$(git show --decorate HEAD --format=%d --decorate=full |
		head -n1 | tr ' (),' '\n\n\n\n' |
		grep ^refs/remotes | grep -v ^refs/remotes/m/ |
		cut -d/ -f4- | head -n1	)"
		;;
esac

# sanitize version and branch
branch="$(echo "${branch:-no_branch}" | tr -d '()[]{}' | tr '/ ' '-_')"
version="$(echo "$version" | tr -d '()[]{}' | tr '/ ' '-_')"

# $version might not include the branch name
# $version1 always includes the branch
# $version2 assumes master when it's a perfect match of a tag

version1=$(echo "$version" |
	   sed -ne "s|\(.*-[0-9]\+\)\(-g[0-9a-f]\+\)\(-dirty\)\?\$|\1-$branch\2\3|p")
if [ -z "$version1" ]; then
	version1=$(echo "$version" | sed -e "s|\(-dirty\)\?\$|-$branch\1|")
fi

if ! git show-ref "$version_tag_ref" > /dev/null; then
	# not exactly on a tag, so append branch info.
	version="$version1"
	version2="$version1"
else
	# exact tag, blame master
	version2=master-$version
fi

# package
package="$(git config --list | sed -n -e 's/\.git$//' -e 's|^remote.[^=]*.url=.*/||p' | head -n1)"

hanover_web="www.hanoverdisplays.com"
hanover_url="http://$hanover_web"
configure_ac_init=`grep AC_INIT configure.ac`
ac_init="AC_INIT([$package], [$version], [support@hanoverdisplays.com], [$package], [$hanover_url])"
ac_init1="AC_INIT([$package], [$version1], [support@hanoverdisplays.com], [$package], [$hanover_url])"
ac_init2="AC_INIT([$package], [$version2], [support@hanoverdisplays.com], [$package], [$hanover_url])"
ac_init3="AC_INIT([$package], [$version2], [support@hanoverdisplays.com], [$package], [$hanover_web])"
cat <<EOT
configure_ac_init=$configure_ac_init
          ac_init=$ac_init
EOT

if [ -n "$configure_ac_init" -a \
	"$configure_ac_init" != "$ac_init" -a \
	"$configure_ac_init" != "$ac_init1" -a \
	"$configure_ac_init" != "$ac_init2" -a \
	"$configure_ac_init" != "$ac_init3" ]; then
	sed -i -e "s|^AC_INIT.*|$ac_init|" configure.ac
fi
