
# Docker Image

The docker image is based on `Ubuntu:12.04` and includes the dependencies required
for building a Hanover OE tree.

The image resulting of running `./build.sh` (or indirectly `./run.sh`) is tagged using the top
two directories on the path to the working copy.

e.g.: if cloning to `~/projects/docker/hanover/arago-builder` the image will be named `hanover/arago-builder`.

In the case of forking this repository to make a custom builder it's recommened to replace `hanover` with
the _username_, like `amery/arago-builder`.

# Usage

the entry point is `.../run.sh`. `$PWD` when calling it will be bound to /ws inside the container.
Aditionally `~/docker-run-cache/$TAG` will be bound to /cache and `~/docker-run-cache/$TAG-home`
to `/home/$USER`.

## Arguments

if no arguments are given a shell will be spawned and the arago environment preloaded.
if the arguments reference an executable, this tool with be called inside the container.
if not, the arguments will be passed directly to arago's `bb`

## Example

first clone the Dockerfile repo:
```sh
mkdir -p ~/projects/docker/hanover
git clone http://git.hanover.local/amery/docker-arago-builder ~/projects/docker/hanover/arago-builder
```

and symlink ./run.sh into your `$PATH`
```sh
ln -s ~/projects/docker/hanover/arago-builder/run.sh ~/bin/arago-docker-run
```

go into your OE tree and start the build:

```sh
cd ~/oe-dev
arago-docker-run -p classic -m dm814x-ht5
```
