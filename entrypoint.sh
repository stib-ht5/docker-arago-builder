#!/bin/sh

set -eu

# find root of the workspace
find_workspace_root() {
	local d="${1:-/}" x=
	shift

	for x; do
		if [ -s "$d/$x" ]; then
			echo "$d"
			return
		fi
	done

	if [ "$d" != / ]; then
		find_repo_workspace_root "$(dirname "$d")" "$@"
	fi
}

WS="$(find_workspace_root "$WORKSPACE" .repo/manifests.xml arago/conf/local.conf)"

# valid arago workspace?
if [ ! -s "$WS/arago/setenv" ]; then
	echo "Invalid workspace, arago/setenv not found." >&2
	exit 2
fi

if [ -z "${USER_HOME:-}" ]; then
	USER_HOME="/home/$USER_NAME"
fi

# create workspace-friendly user
groupadd -r -g "$USER_GID" "$USER_NAME"
useradd -r -g "$USER_GID" -u "$USER_UID" \
	-s /bin/bash -d "$USER_HOME" "$USER_NAME"

if [ ! -s "$USER_HOME/.profile" ]; then
	find /etc/skel ! -type d | while read x; do
		y="$USER_HOME/${x##/etc/skel/}"
		mkdir -p "${y%/*}"
		cp -a "$x" "$y"
		chown "$USER_NAME:$USER_NAME" "$y"
	done
	chown "$USER_NAME:$USER_NAME" "$USER_HOME"
fi

# downloads directory
#
if [ -z "${DL_DIR:-}" ]; then
	DL_DIR="$WS/downloads"
fi

if [ -L "$DL_DIR" ]; then
	DL_DIR="$(readlink -f "$DL_DIR")"
fi

if [ ! -d "$DL_DIR/" ]; then
	mkdir "$DL_DIR"
	chown "$USER_NAME:$USER_NAME" "$DL_DIR"
fi

# call command, if any
#
if [ $# -gt 0 ]; then
	bb_cmd="bb $*"
	cmd="exec $*"

	case "$1" in
	./*|/*)
		;;
	-*)
		cmd="$bb_cmd"
		;;
	*)
		if [ ! -x "`which "$1"`" ]; then
			cmd="$bb_cmd"
		fi
	esac
else
	cmd=
fi

F=/etc/profile.d/Z99-docker-run.sh

# jump to the old $PWD if needed
#
cat <<EOT > "$F"
cd '$WS'
source arago/setenv
EOT

if [ "$WORKSPACE" != "$WS" ]; then
	echo "cd '$WORKSPACE'" >> "$F"
fi

if [ -n "$cmd" ]; then
	echo "exec $cmd" >> "$F"
fi

grep -n ^ "$F"
set -x
su - "$USER_NAME"
