#!/bin/sh
set -eu

# select image
DOCKER_DIR="$(dirname "$(readlink -f "$0")")"

# preserve user identity
USER_NAME=$(id -urn)
USER_UID=$(id -ur)
USER_GID=$(id -gr)

# build image
docker build --rm "$DOCKER_DIR"
DOCKER_ID="$(docker build --rm -q "$DOCKER_DIR")"

# prepend arguments
set -- \
	-e USER_HOME="$HOME" \
	-e USER_NAME=${USER_NAME} \
	-e USER_UID=${USER_UID} \
	-e USER_GID=${USER_GID} \
	"${DOCKER_ID##* }" "$@"

# find root of the workspace
find_workspace_root() {
	local d="${1:-/}" x=
	shift

	for x; do
		if [ -s "$d/$x" ]; then
			echo "$d"
			return
		fi
	done

	if [ "$d" != / ]; then
		find_repo_workspace_root "$(dirname "$d")" "$@"
	fi
}

WS="$(find_workspace_root "$PWD" .repo/manifests.xml arago/conf/local.conf)"

if [ -z "$WS" ]; then
	echo "Invalid workspace." >&2
	exit 2
fi

# persistent volumes
cache_dir="$WS/.docker-run-cache/cache"
home_dir="$WS/.docker-run-cache/home/$USER_NAME"
parent_dir="$(dirname "$PWD")"

for x in "$cache_dir" "$home_dir"; do
	[ -d "$x" ] || mkdir -p "$x"
done

# repo init --reference
REPO_REF_DIR=
if [ -s "$WS/.repo/manifests.git/config" ]; then
	x="$(git config --file "$WS/.repo/manifests.git/config" repo.reference || echo)"
	if [ -n "x" -a -d "$x" ]; then
		REPO_REF_DIR="$(cd "$x" && pwd)"
	fi
fi

# downloads/
[ -n "${DL_DIR:-}" ] || DL_DIR="$WS/downloads"
[ -d "$DL_DIR/" ] || mkdir -p "$DL_DIR"

# environment variables
set -- \
	-e DL_DIR="$DL_DIR" \
	-e WORKSPACE="$PWD" \
	"$@"

# bind volumes uniquely and in the right order
volumes() {
	local x=
	sort -uV | while read x; do
		case "$x" in
		"$HOME")
			echo "-v $home_dir:$x"
			;;
		/cache)
			echo "-v $cache_dir:$x"
			;;
		*)
			if [ -n "$x" -a -d "$x/" ]; then
				echo "-v $(readlink -f "$x"):$x"
			fi
			;;
		esac
	done
}

set -- $(volumes <<EOT
/cache
/usr/share/oe/downloads
$parent_dir
$HOME
$PWD
$DL_DIR
$REPO_REF_DIR
EOT
) "$@"

# and finally run within the container
set -x
exec docker run --rm -ti "$@"
